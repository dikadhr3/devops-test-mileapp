FROM fpm-mileapp
LABEL mainteiner="dikadhr3@gmail.com;"

# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/html/

WORKDIR /var/www/html/

# Add UID '1000' to www-data
RUN usermod -u 1000 www-data

# Copy existing application directory permissions
COPY --chown=www-data:www-data . /var/www/html

# Change current user to www
USER www-data

# RUN php composer.phar install --no-dev --no-scripts
    
COPY . /var/www/html/

RUN cp .env.example .env

# Expose port 5757 and start php-fpm server
EXPOSE 5757
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf
CMD ["php-fpm"]